# Gulp 4 Sass-Browser-Sync

Un despliegue muy simple para compilar Sass y refrescar automáticamente el navegador cuando hay cambios en los archivos `scss`, `html` o `js`.

Tiene soporte adicional con **autoprefixer**, genera **sourcemaps**.

El css se minifica con [postcss-clean](https://github.com/jakubpawlowicz/clean-css), que tiene [muchas opciones](https://github.com/jakubpawlowicz/clean-css#constructor-options).